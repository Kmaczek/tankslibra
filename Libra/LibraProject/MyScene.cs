﻿#region Using Statements
using System;
using System.Collections.Generic;
using LibraProject.Common;
using LibraProject.Device;
using LibraProject.EntityBehavior;
using LibraProject.Helpers;
using LibraProject.Logic;
using LibraProject.PowerActions;
using WaveEngine.Common;
using WaveEngine.Common.Graphics;
using WaveEngine.Common.Math;
using WaveEngine.Components.Cameras;
using WaveEngine.Components.UI;
using WaveEngine.Framework;
using WaveEngine.Framework.Services;
using WaveEngine.Framework.Diagnostic;
using WaveEngine.Framework.UI;

#endregion

namespace LibraProject
{
    public class MyScene : Scene
    {
        private readonly List<TankCore> _tankCores = new List<TankCore>(6);
        private readonly List<Power> _powers = new List<Power>(10);
        private Map _map;

        public Map Map
        {
            get { return _map; }
        }

        public List<TankCore> TankCores
        {
            get { return _tankCores; }
        }

        public FreePositionFinder PositionFinder;

        private void InitializeFields()
        {
            _map = new Map(this);

            Map.GenerateMap();
            Map.RegisterMap();
            PositionFinder = new FreePositionFinder(Map);
            

            var tank = new TankEntity("Mark One", ETankSprite.BigGreen)
            {
                KeySet = ControlsManager.TakeControlState(1)
            };
            var tankCore = new TankCore(tank, this);
            tankCore.States[CoreState.Transparent] = true;
            AddTankCores(tankCore);
            tankCore.Tank.SetStartingPosition(PositionFinder.GetNextFreePosition());


            var tigerTank = new TankEntity("Tiger", ETankSprite.BigOrange)
            {
                KeySet = ControlsManager.TakeControlState(2)
            };
            tigerTank.SetStartingPosition(PositionFinder.GetNextFreePosition());
            var tigerTankCore = new TankCore(tigerTank, this);
            AddTankCores(tigerTankCore);

            var power = new Power("Star");
            AddPower(power);
        }

        protected override void CreateScene()
        {
            WaveServices.ScreenContextManager.SetDiagnosticsActive(true);

            var camera2D = new FixedCamera2D("Camera2D")
            {
                BackgroundColor = Color.CornflowerBlue
            };
            EntityManager.Add(camera2D);

            //Insert your code here
            InitializeFields();
        }

        public void AddTankCores(TankCore core)
        {
            core.TankID = TankCores.Count;
            core.FiredProjectile += TankOnShot;
            TankCores.Add(core);
            core.UpdateInfoThickness();
            EntityManager.Add(core.Tank.Entity);
        }

        private void TankOnShot(object sender, EventArgs args)
        {
            var projectile = sender as Projectile;

            if (projectile != null)
            {
                this.EntityManager.Add(projectile.Entity);
            }
        }

        public void AddPower(Power power)
        {
            power.Expired += PowerActionOnExpired;
            power.AddToMap(this);
            _powers.Add(power);
            EntityManager.Add(power.Entity);
        }

        public void RemovePower(Power power)
        {
            power.Expired -= PowerActionOnExpired;
            EntityManager.Remove(power.Entity);
            _powers.Remove(power);
            power = null;
        }

        private void PowerActionOnExpired(object sender, PowerActionEventArgs eventArgs)
        {
            if (eventArgs.Power.Entity != null)
            {
                EntityManager.Remove(eventArgs.Power.Entity);
                _powers.Remove(eventArgs.Power);
                eventArgs.Power = null;
            }
        }
    }
}
