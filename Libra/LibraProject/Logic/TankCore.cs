﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using LibraProject.Common;
using LibraProject.EntityBehavior;
using LibraProject.EntityBehavior.ProjectileBehavior;
using LibraProject.Helpers;
using LibraProject.Logic.Weapons;
using WaveEngine.Components.UI;
using WaveEngine.Framework;
using WaveEngine.Framework.Services;
using WaveEngine.Framework.UI;

namespace LibraProject.Logic
{
    public delegate void DestroyedEventHandler();
    public delegate void FireProjectileHandler(object sender, EventArgs args);

    public class TankCore
    {
        // public
        public int TankID { get; set; }
        public MyScene Scene { get; set; }

        // private
        private Dictionary<TankProperty, dynamic> _properties;

        #region Events

        // Occurs when tank current health value reaches zero or less
        public event DestroyedEventHandler Destroyed;
        // Occurs whenever tank fires new projectile
        public event FireProjectileHandler FiredProjectile;

        #endregion

        #region Properties

        public int CurrentHealth
        {
            get { return _properties[TankProperty.CurrentHealth]; }
            set
            {
                _properties[TankProperty.CurrentHealth] = value;

                // if health droped below 0, send event
                if (value <= 0)
                {
                    DoDestroyedActions();
                    OnDestroyed();
                }
            }
        }
        public int MaxHealth
        {
            get { return _properties[TankProperty.MaxHealth]; }
            set
            {
                _properties[TankProperty.MaxHealth] = value;

            }
        }
        public float ReloadTime
        {
            get { return _properties[TankProperty.ReloadTime]; }
            set { _properties[TankProperty.ReloadTime] = value; }
        }
        public float Speed
        {
            get { return _properties[TankProperty.Speed]; }
            set { _properties[TankProperty.Speed] = value; }
        }
        public float RespawnTime
        {
            get { return _properties[TankProperty.RespawnTime]; }
            set { _properties[TankProperty.RespawnTime] = value; }
        }
        public TankEntity Tank { get; set; }
        public TextBlock TankInfo { get; set; }
        public WeaponBase Weapon { get; set; }

        public Dictionary<CoreState, bool> States = new Dictionary<CoreState, bool>();

        #endregion

        #region Constructors

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="tank"></param>
        /// <param name="scene"></param>
        public TankCore(TankEntity tank, MyScene scene)
        {
            this.Tank = tank;
            this.Scene = scene;
            InitializeProperties();
            InitializeStates();
            Weapon = WeaponFactory.GetBasicTestCannon(this);

            // Create tank text meka separate class out of this later
            TankInfo = new TextBlock("Text " + Tank.Name)
            {
                Margin = new Thickness(Map.MapWidth + 20, TankID * 50, 0, 0),
                FontPath = PathHelper.ContentPathFactory(PathType.Font, "segoepr"),
                Text = Tank.Name + " HP: " + this.CurrentHealth + "/" + this.MaxHealth,
                Height = 30
            };

            Scene.EntityManager.Add(TankInfo);

            // Order is important. If UpdateInfo is first text will be update with data not yet changed
            // data will then change when OnTankHit is called. On next event UpdateInfo will update text
            // with data from previous event.
            Tank.TankHit += OnTankHit;
            Tank.TankHit += UpdateInfo;

            Tank.ControlsSet += LoadBehavior;
            //Destroyed += DoDestroyedActions;

            LoadBehavior(new object(), null);
        }

        #endregion

        private void InitializeStates()
        {
            States.Add(CoreState.Destroyed, false);
            States.Add(CoreState.Transparent, false);
        }

        private void OnTankHit(object sender, EventArgs args)
        {
            Projectile prj = sender as Projectile;
            CurrentHealth -= prj.Weapon.ProjectileCore.Damage;

            if (Tank.KeySet.Binding == EKeyBinding.Pad)
            {
                Tank.Pad.Vibrate(2);
            }
        }

        private void DoDestroyedActions()
        {
            States[CoreState.Destroyed] = true;
            Tank.Transform.X = -100;
            Tank.Transform.Y = -100;
            this.CurrentHealth = this.MaxHealth;

            var timer = WaveServices.TimerFactory.CreateTimer(
                    "SpawnTimer" + Tank.Name,
                    TimeSpan.FromSeconds(ReloadTime),
                    Spawn,
                    false);
        }

        private void Spawn()
        {
            Tank.Transform.Position = Scene.PositionFinder.GetFreePosition();
            States[CoreState.Destroyed] = false;
        }

        //TODO: set atribute debug mode, when progeam not in debug then dont display this
        public void UpdateInfoThickness()
        {
            TankInfo.Margin = new Thickness(Map.MapWidth + 20, TankID * 50, 0, 0);
        }

        public void UpdateInfo(object sender, EventArgs args)
        {
            TankInfo.Text = Tank.Name + " HP: " + this.CurrentHealth + "/" + this.MaxHealth;
        }

        private void LoadBehavior(object sender, EventArgs args)
        {
            // create behavior
            var behavior = new TankBehavior(this);
            Tank.Entity.RemoveComponent<Behavior>();
            Tank.Entity.AddComponent(behavior);
        }

        public void FireProjectile()
        {
            // Create projectile
            Weapon.FireProjectile();
            //Projectiles.Add(Weapon.NextProjectile());

            // Fire event
            OnFireProjectile();
        }

        /// <summary>
        /// Sender is of type Projectile
        /// </summary>
        protected virtual void OnFireProjectile()
        {
            FireProjectileHandler handler = FiredProjectile;
            if (handler != null) handler(this.Weapon.ProjectileCore.Projectiles.Last(), EventArgs.Empty);
        }

        /// <summary>
        /// Temporary method that is setting test values to core
        /// </summary>
        private void InitializeProperties()
        {
            _properties = new Dictionary<TankProperty, dynamic>();
            _properties.Add(TankProperty.MaxHealth, 100);
            _properties.Add(TankProperty.CurrentHealth, 100);
            _properties.Add(TankProperty.Speed, 3.0f);
            _properties.Add(TankProperty.ReloadTime, 0.8f);
            _properties.Add(TankProperty.RespawnTime, 3f);
        }

        protected virtual void OnDestroyed()
        {
            DestroyedEventHandler handler = Destroyed;
            if (handler != null) handler();
        }
    }

    public enum TankProperty
    {
        MaxHealth,
        CurrentHealth,
        ReloadTime,
        Speed,
        RespawnTime
    }

    // Weapon -> Projectile -> PeojectileBehavior
}
