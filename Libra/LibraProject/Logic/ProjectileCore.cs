﻿using System.Collections.Generic;
using LibraProject.Common;
using LibraProject.EntityBehavior.ProjectileBehavior;
using LibraProject.Helpers;
using LibraProject.Logic.Weapons;

namespace LibraProject.Logic
{
    public class ProjectileCore
    {
        private int _damage;
        private float _speed;
        private int _projectileLimit;
        private WeaponBase _weapon;

        public int Damage
        {
            get { return _damage; }
            set { _damage = value; }
        }
        public int ProjectileLimit
        {
            get { return _projectileLimit; }
            set { _projectileLimit = value; }
        }
        public float Speed
        {
            get { return _speed; }
            set { _speed = value; }
        }

        public List<Projectile> Projectiles = new List<Projectile>(10);

        public ProjectileCore(WeaponBase weapon)
        {
            _weapon = weapon;
            InitWithDefaultValues();
        }

        public void InitWithDefaultValues()
        {
            Damage = 20;
            Speed = 7f;
            ProjectileLimit = 10;
        }

        public void FireProjectile()
        {
            var prj = new Projectile(_weapon, SpriteHelper.GetProjectileSprite(EProjectileSprite.NormalBullet));
            prj.SetBehavior(new BulletBehavior(prj));
            Projectiles.Add(prj);

            _weapon.BulletsFired++;
        }
    }
}
