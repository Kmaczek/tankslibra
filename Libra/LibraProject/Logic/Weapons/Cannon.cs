﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LibraProject.Common;
using LibraProject.EntityBehavior.ProjectileBehavior;

namespace LibraProject.Logic.Weapons
{
    public class Cannon : WeaponBase
    {

        public Cannon(TankCore core)
        {
            TankCore = core;
            ProjectileCore = new ProjectileCore(this);
        }

        public override void FireProjectile()
        {
            ProjectileCore.FireProjectile();
        }

    }
}
