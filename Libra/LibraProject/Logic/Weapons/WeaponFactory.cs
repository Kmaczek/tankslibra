﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LibraProject.Common;

namespace LibraProject.Logic.Weapons
{
    public class WeaponFactory
    {
        private static int _currentId = 0;
        public static WeaponBase GetBasicTestCannon(TankCore tank)
        {
            var cannon = new Cannon(tank)
            {
                Ammo = 20,
                BulletSpeed = 6.0f,
                Id = _currentId,
                MaxAmmo = 32,
                Range = 1000,
                ReloadTime = 0.5f
            };

            _currentId++;
            return cannon;
        }
    }
}
