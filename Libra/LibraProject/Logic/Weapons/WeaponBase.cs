﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LibraProject.Common;

namespace LibraProject.Logic.Weapons
{
    //TODO: possibly make abstract out of this
    public abstract class WeaponBase
    {
        /// <summary>
        /// Identifier
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Speed of projectile
        /// </summary>
        public float BulletSpeed { get; set; }
        /// <summary>
        /// Current ammo, move somwhere else ex. ammo storage if need more logic
        /// </summary>
        public int Ammo { get; set; }
        /// <summary>
        /// Maximum ammo allowed for this weapon type
        /// </summary>
        public int MaxAmmo { get; set; }
        /// <summary>
        /// Delay between each shoot
        /// </summary>
        public float ReloadTime { get; set; }
        /// <summary>
        /// How far bullets travel
        /// </summary>
        public int Range { get; set; }
        /// <summary>
        /// Amount of bullets fired by tank
        /// </summary>
        public int BulletsFired { get; set; }

        public TankCore TankCore { get; set; }
        public ProjectileCore ProjectileCore { get; set; }

        public abstract void FireProjectile();
    }
}
