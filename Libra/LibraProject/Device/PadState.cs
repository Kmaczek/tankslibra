﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharpDX.DirectInput;
using WaveEngine.Common.Input;

namespace LibraProject.Device
{
    public class PadState
    {
        public Dictionary<PadButton, ButtonState> PadKeyStates = new Dictionary<PadButton, ButtonState>();
        private readonly Pad _joystick;
        private const int Middle = 32767;
        private const int Offset = 3000;

        public PadState(Pad joystick)
        {
            _joystick = joystick;
            Initialize();
        }

        public void UpdateState()
        {
            var datas = _joystick.GetBufferedData();
            foreach (var jdata in datas)
            {
                if (jdata.Offset == JoystickOffset.Buttons0)
                {
                    PadKeyStates[PadButton.Triangle] = GetButtonState(jdata.Value);
                }
                if (jdata.Offset == JoystickOffset.Buttons1)
                {
                    PadKeyStates[PadButton.Circle] = GetButtonState(jdata.Value);
                }
                if (jdata.Offset == JoystickOffset.Buttons2)
                {
                    PadKeyStates[PadButton.Cross] = GetButtonState(jdata.Value);
                }
                if (jdata.Offset == JoystickOffset.Buttons3)
                {
                    PadKeyStates[PadButton.Square] = GetButtonState(jdata.Value);
                }
                if (jdata.Offset == JoystickOffset.Buttons4)
                {
                    PadKeyStates[PadButton.L1] = GetButtonState(jdata.Value);
                }
                if (jdata.Offset == JoystickOffset.Buttons5)
                {
                    PadKeyStates[PadButton.R1] = GetButtonState(jdata.Value);
                }
                if (jdata.Offset == JoystickOffset.Buttons6)
                {
                    PadKeyStates[PadButton.L2] = GetButtonState(jdata.Value);
                }
                if (jdata.Offset == JoystickOffset.Buttons7)
                {
                    PadKeyStates[PadButton.R2] = GetButtonState(jdata.Value);
                }
                if (jdata.Offset == JoystickOffset.Buttons8)
                {
                    PadKeyStates[PadButton.Select] = GetButtonState(jdata.Value);
                }
                if (jdata.Offset == JoystickOffset.Buttons9)
                {
                    PadKeyStates[PadButton.Start] = GetButtonState(jdata.Value);
                }
                if (jdata.Offset == JoystickOffset.PointOfViewControllers0)
                {

                    if (jdata.Value == 0)
                    {
                        PadKeyStates[PadButton.Up] = ButtonState.Pressed;
                    }
                    else if (jdata.Value == 9000)
                    {
                        PadKeyStates[PadButton.Right] = ButtonState.Pressed;
                    }
                    else if (jdata.Value == 18000)
                    {
                        PadKeyStates[PadButton.Down] = ButtonState.Pressed;
                    }
                    else if (jdata.Value == 27000)
                    {
                        PadKeyStates[PadButton.Left] = ButtonState.Pressed;
                    }
                    else
                    {
                        PadKeyStates[PadButton.Left] =
                            PadKeyStates[PadButton.Right] =
                                PadKeyStates[PadButton.Up] =
                                    PadKeyStates[PadButton.Down] = ButtonState.Release;
                    }
                }
                if (jdata.Offset == JoystickOffset.X)
                {
                    if (jdata.Value >= Middle + Offset)
                    {
                        PadKeyStates[PadButton.Right] = ButtonState.Pressed;
                    }
                    else if (jdata.Value <= Middle - Offset)
                    {
                        PadKeyStates[PadButton.Left] = ButtonState.Pressed;
                    }
                    else
                    {
                        PadKeyStates[PadButton.Left] =
                            PadKeyStates[PadButton.Right] = ButtonState.Release;
                    }
                }
                if (jdata.Offset == JoystickOffset.Y)
                {
                    if (jdata.Value >= Middle + Offset)
                    {
                        PadKeyStates[PadButton.Down] = ButtonState.Pressed;
                    }
                    else if (jdata.Value <= Middle - Offset)
                    {
                        PadKeyStates[PadButton.Up] = ButtonState.Pressed;
                    }
                    else
                    {
                        PadKeyStates[PadButton.Up] =
                            PadKeyStates[PadButton.Down] = ButtonState.Release;
                    }
                }
            }
        }

        private ButtonState GetButtonState(int value)
        {
            if (value == 128)
            {
                return ButtonState.Pressed;
            }
            return ButtonState.Release;
        }

        private void Initialize()
        {
            PadKeyStates.Add(PadButton.Triangle, ButtonState.Release);
            PadKeyStates.Add(PadButton.Circle, ButtonState.Release);
            PadKeyStates.Add(PadButton.Cross, ButtonState.Release);
            PadKeyStates.Add(PadButton.Square, ButtonState.Release);
            PadKeyStates.Add(PadButton.Up, ButtonState.Release);
            PadKeyStates.Add(PadButton.Right, ButtonState.Release);
            PadKeyStates.Add(PadButton.Down, ButtonState.Release);
            PadKeyStates.Add(PadButton.Left, ButtonState.Release);
            PadKeyStates.Add(PadButton.L1, ButtonState.Release);
            PadKeyStates.Add(PadButton.L2, ButtonState.Release);
            PadKeyStates.Add(PadButton.R1, ButtonState.Release);
            PadKeyStates.Add(PadButton.R2, ButtonState.Release);
            PadKeyStates.Add(PadButton.Select, ButtonState.Release);
            PadKeyStates.Add(PadButton.Start, ButtonState.Release);
        }
    }

    public enum PadButton
    {
        None,
        Triangle,
        Circle,
        Cross,
        Square,
        Up,
        Right,
        Down,
        Left,
        L1,
        L2,
        R1,
        R2,
        Select,
        Start
    }
}
