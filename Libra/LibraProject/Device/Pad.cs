﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Forms.VisualStyles;
using LibraProject.Helpers;
using SharpDX.DirectInput;
using WaveEngine.Framework.Services;

namespace LibraProject.Device
{
    public class Pad
    {
        public int BufferSize
        {
            get { return _joystick.Properties.BufferSize; }
            set { _joystick.Properties.BufferSize = value; }
        }
        public PadState PadState { get; set; }
        public Guid Guid
        {
            get { return _joystick.Information.InstanceGuid; }
        }
        public String Name
        {
            get
            {
                return _joystick.Information.InstanceName;
            }
        }
        private EffectFile EffectFile
        {
            get
            {
                if (_effectFile == null)
                {
                    LoadEffectFile();
                }
                return _effectFile;
            }
        }
        private Effect Effect
        {
            get
            {
                if (_effect == null)
                {
                    _effect = new Effect(_joystick, EffectFile.Guid, EffectFile.Parameters);
                }

                return _effect;
            }
        }

        private static EffectFile _effectFile;
        private Effect _effect;
        private Joystick _joystick;

        public Pad(Guid guid)
        {
            CreatePad(guid);
        }

        private void CreatePad(Guid guid)
        {
            _joystick = new Joystick(ControlsManager.DirectInput, guid);
            BufferSize = 128;
            _joystick.SetCooperativeLevel(Game.WinHandle, CooperativeLevel.Exclusive | CooperativeLevel.Foreground);
            _joystick.Properties.AxisMode = DeviceAxisMode.Absolute;
            _joystick.Properties.AutoCenter = false;
            _joystick.Acquire();
        }

        private void LoadEffectFile()
        {
            _effectFile = _joystick.GetEffectsInFile(
                @"E:\Projekty\Libra\Libra\LibraProject\ForceFeedbackEffects\const100.ffe",
                EffectFileFlags.ModidyIfNeeded).First();
        }

        public void Vibrate(int iterations)
        {
            Effect.Start(iterations);
        }

        public void StopVibration()
        {
            if (_effect.Status == EffectStatus.Playing)
            {
                Effect.Stop();
            }

        }

        public String GetInfo()
        {
            var cap = _joystick.Capabilities;
            var stringBuilder = new StringBuilder();
            stringBuilder.AppendLine("AxeCount: " + cap.AxeCount)
                .AppendLine("Button count: " + cap.ButtonCount)
                .AppendLine("Flags: " + cap.Flags)
                .AppendLine("FFSamplePeriod: " + cap.ForceFeedbackSamplePeriod)
                .AppendLine("Human intf device: " + cap.IsHumanInterfaceDevice)
                .AppendLine("Pov count: " + cap.PovCount)
                .AppendLine("Subtype: " + cap.Subtype)
                .AppendLine("Type: " + cap.Type);

            return stringBuilder.ToString();
        }

        public List<JoystickUpdate> GetBufferedData()
        {
            var list = new List<JoystickUpdate>(10);

            try
            {
                if (Game.Form.Focused)
                {
                    var datas = _joystick.GetBufferedData();
                    foreach (var joystickUpdate in datas)
                    {
                        list.Add(joystickUpdate);
                    }
                }
            }
            catch (Exception ex)
            {
                if (Game.Form.Focused)
                {
                    _joystick.Acquire();
                }
            }

            return list;
        }

        public List<String> GetEffects()
        {
            var list = new List<String>(10);
            foreach (var eff in _joystick.GetEffects())
            {
                list.Add(eff.Name);
            }

            return list;
        }
    }
}
