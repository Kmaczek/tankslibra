﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LibraProject.Common;
using LibraProject.Device;
using SharpDX.DirectInput;

namespace LibraProject.Device
{
    public static class ControlsManager
    {
        public static DirectInput DirectInput
        {
            get { return _directInput ?? (_directInput = new DirectInput()); }
            set { _directInput = value; }
        }
        public static List<Guid> PadGuids
        {
            get
            {
                if (_freeGamepadGuids == null || _freeGamepadGuids.Count <= 0)
                {
                    _freeGamepadGuids = new List<Guid>(4);

                    foreach (var deviceInstance in DirectInput.GetDevices(DeviceType.Gamepad, DeviceEnumerationFlags.AllDevices))
                    {
                        _freeGamepadGuids.Add(deviceInstance.InstanceGuid);
                    }

                    if (!_freeGamepadGuids.Any())
                    {
                        foreach (var deviceInstance in DirectInput.GetDevices(DeviceType.Joystick, DeviceEnumerationFlags.AllDevices))
                        {
                            _freeGamepadGuids.Add(deviceInstance.InstanceGuid);
                        }
                    }
                }
                return _freeGamepadGuids;
            }
            set { _freeGamepadGuids = value; }
        }
        public static bool HasPadGuids
        {
            get
            {
                if (_freeGamepadGuids == null || _freeGamepadGuids.Count <= 0)
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
        }
        public static List<ControlState> ControlList
        {
            get { return _controlsList; }
            set { _controlsList = value; }
        }

        public static int DevicesAmount = 0;

        private static DirectInput _directInput;
        private static List<Guid> _freeGamepadGuids;
        private static List<ControlState> _controlsList = new List<ControlState>(10);
        private static bool _initialized;

        public static ControlState TakeControlState(int id)
        {
            if (!HasPadGuids || !_initialized)
            {
                Initialize();
            }

            var ctr = ControlList.First(x => x.Id == id);
            ctr.Taken = true;
            
            return ctr;
        }

        private static void Initialize()
        {
            DevicesAmount++;
            _controlsList.Add(new ControlState()
            {
                Binding = EKeyBinding.Arrows,
                Taken = false,
                PadGuid = Guid.Empty,
                Id = DevicesAmount
            });

            DevicesAmount++;
            _controlsList.Add(new ControlState()
            {
                Binding = EKeyBinding.WASD,
                Taken = false,
                PadGuid = Guid.Empty,
                Id = DevicesAmount
            });

            foreach (var freePadGuid in ControlsManager.PadGuids)
            {
                DevicesAmount++;
                _controlsList.Add(new ControlState()
                {
                    Binding = EKeyBinding.Pad,
                    Taken = false,
                    PadGuid = freePadGuid,
                    Id = DevicesAmount
                });
            }

            _initialized = true;
        }
    }

    public struct ControlState
    {
        public int Id { get; set; }
        public bool Taken { get; set; }
        public EKeyBinding Binding { get; set; }
        public Guid PadGuid { get; set; }
    }
}
