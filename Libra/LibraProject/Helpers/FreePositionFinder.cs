﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LibraProject.Common;
using LibraProject.Common.Tiles;
using WaveEngine.Common.Math;

namespace LibraProject.Helpers
{
    public class FreePositionFinder
    {
        private List<SpawnTile> _positions = new List<SpawnTile>(10);
        private int _currentFree = 0;
        Random rng = new Random(DateTime.Now.Millisecond);

        public FreePositionFinder(Map map)
        {
            _positions = map.GetSpawnTiles();
        }

        public Vector2 GetFreePosition()
        {
            var freeList = new List<SpawnTile>();

            foreach (SpawnTile position in _positions)
            {
                if (position.IsFree)
                {
                    freeList.Add(position);
                }
            }
            int freePositionIndex = rng.Next(freeList.Count);

            return freeList[freePositionIndex].Transform.Position.Add(Tile.WidthOffset, Tile.HeightOffset);
        }

        public Vector2 GetNextFreePosition()
        {
            if (_currentFree > _positions.Count)
            {
                return new Vector2(0, 0);
            }
            var retVec = new Vector2(_positions[_currentFree].Transform.X, _positions[_currentFree].Transform.Y);
            _currentFree++;

            return retVec;
        }
    }
}
