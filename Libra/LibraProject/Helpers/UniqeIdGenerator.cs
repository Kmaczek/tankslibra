﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibraProject.Helpers
{
    public static class UniqeIdGenerator
    {
        private static Dictionary<string, IUnique> _dict = new Dictionary<string, IUnique>();

        public static void AddList(string name, IUnique list)
        {
            if (!_dict.Keys.Contains(name))
            {
                _dict.Add(name, list);
            }
        }

        public static dynamic Next(string name)
        {
            return _dict[name].Next();
        }
    }

    public class NameList: IUnique
    {
        private List<string> _list = new List<string>();
        private string _prefix;

        public NameList(string prefix)
        {
            _prefix = prefix;
        }

        public dynamic Next()
        {
            var ret = _prefix + _list.Count + 1;
            _list.Add(ret);

            return ret;
        }
    }

    public class IdList : IUnique
    {
        private static int _current = 0;

        public dynamic Next()
        {
            _current++;
            return _current;
        }
    }

    public interface IUnique
    {
        dynamic Next();
    }
}
