﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibraProject.Helpers
{
    public static class PathHelper
    {
        public const String ContentPath = "Content/";
        public const String Other = "Other/";
        public const String Power = "Powers/";
        public const String Projectile = "Projectiles/";
        public const String Tank = "Tanks/";
        public const String Tile = "Tiles/";
        public const String Font = "Fonts/";

        public const String WpkExtension = ".wpk";

        public static String ContentPathFactory(PathType pathType, String name)
        {
            switch (pathType)
            {
                case PathType.Tile:
                    return ContentPath + Tile + name.ToLowerInvariant() + WpkExtension;
                case PathType.Tank:
                    return ContentPath + Tank + name.ToLowerInvariant() + WpkExtension;
                case PathType.Power:
                    return ContentPath + Power + name.ToLowerInvariant() + WpkExtension;
                case PathType.Projectile:
                    return ContentPath + Projectile + name.ToLowerInvariant() + WpkExtension;
                case PathType.Font:
                    return ContentPath + Font + name.ToLowerInvariant() + WpkExtension;
                case PathType.Other:
                    return ContentPath + Other + name.ToLowerInvariant() + WpkExtension;
                default:
                    throw new Exception("Usage of not implemented enum PathType value.");
            }
        }


    }

    public enum PathType
    {
        Font,
        Power,
        Projectile,
        Tank,
        Tile,
        Other
    }
}
