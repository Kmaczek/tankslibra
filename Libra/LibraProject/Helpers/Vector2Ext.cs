﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WaveEngine.Common.Math;

namespace LibraProject.Helpers
{
    public static class Vector2Ext
    {
        public static Vector2 Add(this Vector2 vector, float x, float y)
        {
            Vector2 replacement = new Vector2(
                vector.X + x,
                vector.Y + y);
            vector = replacement;

            return vector;
        }
    }
}
