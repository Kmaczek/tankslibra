﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WaveEngine.Components.Graphics2D;

namespace LibraProject.Helpers
{
    public static class SpriteHelper
    {
        public static Sprite GetTankSprite(ETankSprite tankSprite)
        {
            switch (tankSprite)
            {
                case ETankSprite.BigGreen:
                    return new Sprite(PathHelper.ContentPathFactory(PathType.Tank, "big_tank01"));
                case ETankSprite.BigOrange:
                    return new Sprite(PathHelper.ContentPathFactory(PathType.Tank, "big_tank02"));
                case ETankSprite.SmallBrown:
                    return new Sprite(PathHelper.ContentPathFactory(PathType.Tank, "small_tank01"));
                default:
                    return new Sprite(PathHelper.ContentPathFactory(PathType.Tank, "big_tank01"));
            }
        }

        public static Sprite GetTileSprite(ETileSprite tileSprite)
        {
            switch (tileSprite)
            {
                case ETileSprite.Grass:
                    return new Sprite(PathHelper.ContentPathFactory(PathType.Tile, "grass01"));
                case ETileSprite.GreyFloor:
                    return new Sprite(PathHelper.ContentPathFactory(PathType.Tile, "floor02"));
                case ETileSprite.RedSteel:
                    return new Sprite(PathHelper.ContentPathFactory(PathType.Tile, "floor01"));
                case ETileSprite.SpawnTile:
                    return new Sprite(PathHelper.ContentPathFactory(PathType.Tile, "spawn_tile"));
                default:
                    return new Sprite(PathHelper.ContentPathFactory(PathType.Tile, "floor02"));
            }
        }

        public static Sprite GetProjectileSprite(EProjectileSprite projectileSprite)
        {
            switch (projectileSprite)
            {
                case EProjectileSprite.NormalBullet:
                    return new Sprite(PathHelper.ContentPathFactory(PathType.Projectile, "bullet01"));
                default:
                    return new Sprite(PathHelper.ContentPathFactory(PathType.Projectile, "bullet01"));
            }
        }
    }

    public enum EProjectileSprite
    {
        NormalBullet
    }

    public enum ETankSprite
    {
        BigGreen,
        BigOrange,
        SmallBrown
    }

    public enum ETileSprite
    {
        Grass,
        RedSteel,
        GreyFloor,
        SpawnTile
    }

}
