﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibraProject.Helpers
{
    public enum MovementIndicator
    {
        Idle, 
        Up, 
        Down, 
        Left, 
        Right
    };

    public enum Facing
    {
        N,
        S,
        W,
        E
    }
}
