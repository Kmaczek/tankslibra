﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LibraProject.Common;
using LibraProject.Logic;

namespace LibraProject.PowerActions
{
    public delegate void PowerExpiredHandler(object sender, PowerActionEventArgs eventArgs);

    public interface IPowerAction
    {
        event PowerExpiredHandler Expired;

        void Apply(TankCore tank);
        void Expire(TankCore tank);
    }
}
