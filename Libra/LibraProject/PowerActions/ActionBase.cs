﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LibraProject.Logic;
using WaveEngine.Framework.Graphics;
using WaveEngine.Framework.Services;

namespace LibraProject.PowerActions
{
    abstract public class ActionBase
    {
        public event PowerExpiredHandler Expired;

        public float Duration
        {
            get { return _duration; }
            set { _duration = value; }
        }

        public TankCore TankCore { get; set; }

        private float _duration;
        private Timer _timer;

        protected virtual void OnExpired(PowerActionEventArgs eventargs)
        {
            PowerExpiredHandler handler = Expired;
            if (handler != null) handler(this, eventargs);
        }

        protected ActionBase()
        {
            _duration = 3f;
        }

        protected ActionBase(float duration)
        {
            Duration = duration;
        }

        public virtual void Apply(TankCore tank)
        {
            TankCore = tank;
            StartTimer(Duration);
        }

        public virtual void Expire(TankCore tank)
        {
            OnExpired(null);
        }

        private void StartTimer(float duration)
        {
            _timer = WaveServices.TimerFactory.CreateTimer(
                            TankCore.Tank.Name + "Timer",
                            TimeSpan.FromSeconds(duration),
                            () => Expire(TankCore),
                            false);
        }
    }
}
