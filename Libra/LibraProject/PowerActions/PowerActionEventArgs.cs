﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LibraProject.Common;

namespace LibraProject.PowerActions
{
    public class PowerActionEventArgs : EventArgs
    {
        public Power Power { get; set; }

        public PowerActionEventArgs()
        {
        }
    }
}
