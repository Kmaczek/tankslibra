﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using LibraProject.Common;
using LibraProject.Logic;

namespace LibraProject.PowerActions
{
    public class ModifySpeed : ActionBase
    {
        /// <summary>
        /// 
        /// </summary>
        [Range(0, 300)]
        public float PercentModified { get; set; }

        private float _speedBefore;

        public ModifySpeed(int percent)
        {
            PercentModified = percent;

        }

        public ModifySpeed(int percent, float duration) : base(duration)
        {
            PercentModified = percent;
        }

        public override void Apply(TankCore core)
        {
            base.Apply(core);

            _speedBefore = core.Speed;
            core.Speed = core.Speed * (PercentModified / 100);
        }

        public override void Expire(TankCore core)
        {
            core.Speed = _speedBefore;

            base.Expire(core);
        }
    }
}
