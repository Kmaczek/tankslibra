﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LibraProject.Common;
using LibraProject.Helpers;
using LibraProject.Logic;

namespace LibraProject.PowerActions
{
    public class InvulnerableAction : ActionBase
    {
        public InvulnerableAction()
        {
            
        }

        public InvulnerableAction(float duration) : base(duration)
        {
            
        }

        public override void Apply(TankCore tank)
        {
            base.Apply(tank);

            tank.States[CoreState.Invurnelable] = true;
        }

        public override void Expire(TankCore tank)
        {
            tank.States[CoreState.Invurnelable] = false;

            base.Expire(tank);
        }
    }
}
