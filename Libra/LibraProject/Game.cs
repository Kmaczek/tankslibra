﻿#region Using Statements
using System;
using System.Diagnostics.Contracts;
using System.Security.Principal;
using System.Windows.Forms;
using WaveEngine.Common;
using WaveEngine.Common.Graphics;
using WaveEngine.Framework;
using WaveEngine.Framework.Services;
#endregion

namespace LibraProject
{
    public class Game : WaveEngine.Framework.Game
    {
        public static IntPtr WinHandle;
        public static Control Form;

        public Game(Control form)
        {
            Form = form;
            WinHandle = form.Handle;
        }

        public override void Initialize(IApplication application)
        {
            base.Initialize(application);


            ScreenContext screenContext = new ScreenContext(new MyScene());
            WaveServices.ScreenContextManager.To(screenContext);
        }
    }
}
