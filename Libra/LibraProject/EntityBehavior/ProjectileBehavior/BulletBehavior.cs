﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LibraProject.Common;
using LibraProject.Common.Tiles;
using LibraProject.Helpers;
using LibraProject.Logic;
using WaveEngine.Common.Math;
using WaveEngine.Components.Graphics2D;
using WaveEngine.Framework;
using WaveEngine.Framework.Graphics;
using WaveEngine.Framework.Physics2D;

namespace LibraProject.EntityBehavior.ProjectileBehavior
{
    public class BulletBehavior : Behavior, IDisposable
    {
        public static int BulletCount = 0;

        [RequiredComponent]
        public Transform2D Transform2D;
        [RequiredComponent]
        public RectangleCollider RectangleCollider;
        [RequiredComponent]                                                                                                                                   
        public Sprite Sprite;

        private Projectile _projectile;
        private Facing _bulletDirection;
        private float _xOffset;
        private float _yOffset;

        public BulletBehavior(Projectile projectile)
        {
            _projectile = projectile;
            _bulletDirection = projectile.TankCore.Tank.Facing;
        }

        protected override void Initialize()
        {
            base.Initialize();
            UpdateFacing();
            _xOffset = Transform2D.Rectangle.Width/2;
        }

        protected override void Update(TimeSpan gameTime)
        {
            UpdatePosition(gameTime);

            CheckBorderCollison();
            CheckTileCollision();
            CheckTankCollision();
        }

        private void CheckBorderCollison()
        {
            if ((Transform2D.X > Map.MapWidth - _xOffset) ||
                (Transform2D.X < _xOffset) ||
                (Transform2D.Y < _yOffset) ||
                (Transform2D.Y > Map.MapHeight - _yOffset))
            {
                Dispose();
            }
        }

        private void CheckTileCollision()
        {
            foreach (BaseTile tile in _projectile.TankCore.Scene.Map.CollisionTiles)
            {
                if (RectangleCollider.Intersects(tile.RectCollider))
                {
                    Dispose();
                }
            }
            
        }

        private void CheckTankCollision()
        {
            foreach (TankCore core in _projectile.TankCore.Scene.TankCores)
            {
                if (!core.Tank.Name.Equals(_projectile.TankCore.Tank.Name))
                {
                    if (RectangleCollider.Intersects(core.Tank.RectCollider))
                    {
                        core.Tank.PerformTankShotEvent(this._projectile);
                        Dispose();
                    }
                }
            }
        }

        private void UpdatePosition(TimeSpan gameTime)
        {
            switch (_bulletDirection)
            {
                case Facing.N:
                    Transform2D.Y -= _projectile.Weapon.ProjectileCore.Speed*(gameTime.Milliseconds/10);
                    break;
                case Facing.S:
                    Transform2D.Y += _projectile.Weapon.ProjectileCore.Speed * (gameTime.Milliseconds / 10);
                    break;
                case Facing.W:
                    Transform2D.X -= _projectile.Weapon.ProjectileCore.Speed * (gameTime.Milliseconds / 10);
                    break;
                case Facing.E:
                    Transform2D.X += _projectile.Weapon.ProjectileCore.Speed * (gameTime.Milliseconds / 10);
                    break;
            }
        }

        private void UpdateFacing()
        {
            switch (_bulletDirection)
            {
                case Facing.N:
                    Sprite.Transform2D.Rotation = 0;
                    break;
                case Facing.S:
                    Sprite.Transform2D.Rotation = MathHelper.Pi;
                    break;
                case Facing.E:
                    Sprite.Transform2D.Rotation = MathHelper.PiOver2;
                    break;
                case Facing.W:
                    Sprite.Transform2D.Rotation = MathHelper.Pi + MathHelper.PiOver2;
                    break;
            }

        }

        public void Dispose()
        {
            //this.Dispose();
            EntityManager.Remove(_projectile.Entity);
            _projectile.TankCore.Weapon.ProjectileCore.Projectiles.Remove(_projectile);
        }
    }
}
