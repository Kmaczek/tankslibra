﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LibraProject.Common;
using LibraProject.Logic;
using LibraProject.PowerActions;
using WaveEngine.Components.Graphics2D;
using WaveEngine.Framework;
using WaveEngine.Framework.Diagnostic;
using WaveEngine.Framework.Graphics;
using WaveEngine.Framework.Physics2D;
using WaveEngine.Framework.Services;

namespace LibraProject.EntityBehavior
{
    public class PowerBehavior : Behavior
    {
        private readonly MyScene _scene;
        private readonly ActionBase _powerAction;
        private TankEntity _appliedTank;

        [RequiredComponent]
        public Transform2D Transform2D;
        [RequiredComponent]
        public RectangleCollider RectangleCollider;
        [RequiredComponent]
        public SpriteRenderer SpriteRenderer;

        public PowerBehavior(MyScene scene, ActionBase powerAction)
        {
            this._scene = scene;
            this._powerAction = powerAction;
        }

        protected override void Update(TimeSpan gameTime)
        {
            if (_appliedTank == null)
            {
                foreach (TankCore core in _scene.TankCores)
                {
                    if (RectangleCollider.Intersects(core.Tank.RectCollider))
                    {
                        _powerAction.Apply(core);
                        _appliedTank = core.Tank;
                        Fade();
                    }
                }
            }
        }

        private void Fade()
        {
            SpriteRenderer.IsVisible = false;
            Transform2D.X = -200;
            Transform2D.Y = -200;
        }

    }
}
