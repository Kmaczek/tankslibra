﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LibraProject.Common;
using LibraProject.Common.Tiles;
using WaveEngine.Framework;

namespace LibraProject.EntityBehavior
{
    public class TileSpawnBehavior : Behavior
    {
        private SpawnTile _tile;
        public TileSpawnBehavior(SpawnTile tile)
        {
            _tile = tile;
        }

        protected override void Update(TimeSpan gameTime)
        {
            foreach (var core in _tile.Scene.TankCores)
            {
                if (core.Tank.RectCollider.Intersects(_tile.RectCollider))
                {
                    _tile.IsFree = false;
                    return;
                }
            }
            _tile.IsFree = true;
        }
    }
}
