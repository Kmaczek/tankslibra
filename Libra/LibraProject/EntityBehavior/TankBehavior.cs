﻿using System;
using System.Collections.Generic;
using System.Globalization;
using LibraProject.Common;
using LibraProject.Common.Tiles;
using LibraProject.Device;
using LibraProject.Helpers;
using LibraProject.Logic;
using WaveEngine.Common.Input;
using WaveEngine.Common.Math;
using WaveEngine.Components.Graphics2D;
using WaveEngine.Framework;
using WaveEngine.Framework.Diagnostic;
using WaveEngine.Framework.Graphics;
using WaveEngine.Framework.Physics2D;
using WaveEngine.Framework.Services;

namespace LibraProject.EntityBehavior
{
    public class TankBehavior : Behavior
    {
        private const int Up = -1;
        private const int Down = 1;
        private const int Left = -1;
        private const int Right = 1;
        private const int None = 0;

        [RequiredComponent]
        public Transform2D Trans2D;
        [RequiredComponent]
        public RectangleCollider TankRectangleCollider;

        private int _direction;
        private MovementIndicator _currentState;
        private MovementIndicator _lastState;
        private Facing _previousFacing;
        private readonly List<BaseTile> _tiles;
        private float _xOffset;
        private float _yOffset;

        private bool _reloading = false;
        private Timer _reloadingTimer;
        PadState _padState;
        private TankCore _core;
        private TankEntity _tank;

        public TankBehavior(TankCore core)
        {
            this._core = core;
            this._tank = core.Tank;
            this._tiles = _core.Scene.Map.Tiles;
            this._direction = None;
            this.Trans2D = null;
            this._currentState = MovementIndicator.Idle;
            if (_tank.KeySet.Binding == EKeyBinding.Pad)
            {
                _padState = new PadState(_tank.Pad);
            }
        }

        protected override void Initialize()
        {
            base.Initialize();

            _xOffset = Trans2D.Rectangle.Width * Trans2D.Origin.X;
            _yOffset = Trans2D.Rectangle.Height * Trans2D.Origin.Y;

            _reloadingTimer = WaveServices.TimerFactory.CreateTimer(
                    "Projectile Timer" + _tank.Name,
                    TimeSpan.FromSeconds(_core.ReloadTime),
                    () => _reloading = false,
                    true);
        }

        protected override void Update(TimeSpan gameTime)
        {
            _currentState = MovementIndicator.Idle;
            if (!_core.States[CoreState.Destroyed])
            {
                // Tank Movement
                HandleKeyboardInput(gameTime);
                SetDirection();
                UpdatePosition(gameTime);
                UpdateFacing();

                //Diagnostics
                Labels.Add("tank", String.Format("X: {0} Y: {1}", Trans2D.X, Trans2D.Y));
                Labels.Add("bulets: ", _core.Weapon.ProjectileCore.Projectiles.Count);
                // Colisions
                CheckBorderCollisions();
                if (!_core.States[CoreState.Transparent])
                {
                    CheckTileCollisions();
                    CheckTankCollisions();
                }
            }
        }

        /// <summary>
        /// Checks collisons between tanks.
        /// </summary>
        private void CheckTankCollisions()
        {
            foreach (TankCore core in _core.Scene.TankCores)
            {
                if (!core.Tank.Name.Equals(_tank.Name) && !core.States[CoreState.Transparent])
                {
                    if (_currentState == MovementIndicator.Left && CheckCollision(core.Tank))
                    {
                        Trans2D.X = core.Tank.Transform.X + core.Tank.Transform.Rectangle.Center.X + 1 + _xOffset;
                    }
                    else if (_currentState == MovementIndicator.Right && CheckCollision(core.Tank))
                    {
                        Trans2D.X = core.Tank.Transform.X - core.Tank.Transform.Rectangle.Center.X - 1 - _xOffset;
                    }
                    else if (_currentState == MovementIndicator.Up && CheckCollision(core.Tank))
                    {
                        Trans2D.Y = core.Tank.Transform.Y + core.Tank.Transform.Rectangle.Center.Y + 1 + _yOffset;
                    }
                    else if (_currentState == MovementIndicator.Down && CheckCollision(core.Tank))
                    {
                        Trans2D.Y = core.Tank.Transform.Y - core.Tank.Transform.Rectangle.Center.X - 1 - _yOffset;
                    }
                }
            }
        }

        /// <summary>
        /// Prevents tank from crossing borders of screen.
        /// </summary>
        private void CheckBorderCollisions()
        {
            if (Trans2D.X > Map.MapWidth - _xOffset)
            {
                Trans2D.X = Map.MapWidth - _xOffset;
            }
            else if (Trans2D.X < _xOffset)
            {
                Trans2D.X = _xOffset;
            }
            else if (Trans2D.Y < _yOffset)
            {
                Trans2D.Y = _yOffset;
            }
            else if (Trans2D.Y > Map.MapHeight - _yOffset)
            {
                Trans2D.Y = Map.MapHeight - _yOffset;
            }
        }

        /// <summary>
        /// Checks if Tank colides with any of Tiles that are CollisionTiles. If
        /// it does, then prevents tank from moving further. Can be 
        /// optimized, by altering List _tiles to Array(!) with collision tiles only.
        /// </summary>
        private void CheckTileCollisions()
        {
            // TODO: Can be optimized
            if (_tiles != null)
            {
                foreach (BaseTile tile in _tiles)
                {
                    if (tile is CollisionTile)
                    {
                        if (_currentState == MovementIndicator.Left && CheckCollision(tile))
                        {
                            Trans2D.X = tile.Transform.X + tile.Transform.Rectangle.Right + _xOffset;
                        }
                        else if (_currentState == MovementIndicator.Right && CheckCollision(tile))
                        {
                            Trans2D.X = tile.Transform.X - _xOffset;
                        }
                        else if (_currentState == MovementIndicator.Up && CheckCollision(tile))
                        {
                            Trans2D.Y = tile.Transform.Y + tile.Transform.Rectangle.Width + _yOffset;
                        }
                        else if (_currentState == MovementIndicator.Down && CheckCollision(tile))
                        {
                            Trans2D.Y = tile.Transform.Y - _yOffset;
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Moves tank depanding on _direction field. 
        /// </summary>
        /// <param name="gameTime">GameTime used by Update method.</param>
        private void UpdatePosition(TimeSpan gameTime)
        {
            if (_currentState == MovementIndicator.Up ||
                _currentState == MovementIndicator.Down)
            {
                Trans2D.Y += _direction * _core.Speed * (gameTime.Milliseconds / 10);
            }
            else if (_currentState == MovementIndicator.Left ||
                     _currentState == MovementIndicator.Right)
            {
                Trans2D.X += _direction * _core.Speed * (gameTime.Milliseconds / 10);
            }
        }

        private void UpdateFacing()
        {
            if (_tank.Facing != _previousFacing)
            {
                switch (_tank.Facing)
                {
                    case Facing.N:
                        _tank.Sprite.Transform2D.Rotation = 0;
                        break;
                    case Facing.S:
                        _tank.Sprite.Transform2D.Rotation = MathHelper.Pi;
                        break;
                    case Facing.E:
                        _tank.Sprite.Transform2D.Rotation = MathHelper.PiOver2;
                        break;
                    case Facing.W:
                        _tank.Sprite.Transform2D.Rotation = MathHelper.Pi + MathHelper.PiOver2;
                        break;
                }
            }

            _previousFacing = _tank.Facing;
        }

        /// <summary>
        /// Sets _direction and facing variables depending on state of MovementIndicator
        /// </summary>
        private void SetDirection()
        {
            if (_currentState != _lastState)
            {
                switch (_currentState)
                {
                    case MovementIndicator.Idle:
                        _direction = None;
                        break;
                    case MovementIndicator.Up:
                        _direction = Up;
                        _tank.Facing = Facing.N;
                        break;
                    case MovementIndicator.Down:
                        _direction = Down;
                        _tank.Facing = Facing.S;
                        break;
                    case MovementIndicator.Left:
                        _direction = Left;
                        _tank.Facing = Facing.W;
                        break;
                    case MovementIndicator.Right:
                        _direction = Right;
                        _tank.Facing = Facing.E;
                        break;
                }
            }

            _lastState = _currentState;
        }

        /// <summary>
        /// Handles KeyboardInput and changes MovementIndicator accordingly
        /// </summary>
        private void HandleKeyboardInput(TimeSpan gameTime)
        {
            var actions = new TankActions(_tank.KeySet.Binding, _padState);

            if (actions.Shoot == ButtonState.Pressed && _reloading == false)
            {
                _core.FireProjectile();
                
                _reloading = true;
            }

            if (actions.MoveLeft == ButtonState.Pressed)
            {
                _currentState = MovementIndicator.Left;
                return;
            }
            if (actions.MoveRight == ButtonState.Pressed)
            {
                _currentState = MovementIndicator.Right;
                return;
            }
            if (actions.MoveTop == ButtonState.Pressed)
            {
                _currentState = MovementIndicator.Up;
                return;
            }
            if (actions.MoveDown == ButtonState.Pressed)
            {
                _currentState = MovementIndicator.Down;
                return;
            }

            if (actions.UsePower == ButtonState.Pressed)
            {

//                _tank.Entity.RemoveComponent<Sprite>();
//                _tank.Entity.RemoveComponent<SpriteRenderer>();
//                _tank.Sprite = new Sprite(PathHelper.ContentPathFactory(PathType.Tank, "big_tank02"));
//                _tank.Entity.AddComponent(_tank.Sprite);
//                _tank.Entity.AddComponent(new SpriteRenderer(DefaultLayers.Alpha));
            }
        }

        /// <summary>
        /// Checks if Tile is coliding with Tank
        /// </summary>
        /// <param name="tile">Pass Tile with RectangleColider component</param>
        /// <returns>True if collision is detected, otherwise false</returns>
        private bool CheckCollision(BaseEntity tile)
        {
            var rect = new RectangleF(Trans2D.X - Trans2D.Rectangle.Width / 2,
                Trans2D.Y - Trans2D.Rectangle.Height / 2,
                Trans2D.Rectangle.Width,
                Trans2D.Rectangle.Height);

            if (tile.RectCollider.Intersects(rect, false))
            {
                return true;
            }

            return false;
        }
    }
}
