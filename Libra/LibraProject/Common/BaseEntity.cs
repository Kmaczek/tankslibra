﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WaveEngine.Components.Graphics2D;
using WaveEngine.Framework;
using WaveEngine.Framework.Graphics;
using WaveEngine.Framework.Physics2D;

namespace LibraProject.Common
{
    public class BaseEntity
    {
        public String Name { get; set; }
        public Entity Entity { get; set; }
        public Sprite Sprite { get; set; }
        public Transform2D Transform { get; set; }
        public RectangleCollider RectCollider { get; set; }
    }
}
