﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibraProject.Common
{
    public class DrawOrder
    {
        public const float Tiles = 0.95f;
        public const float Tank = 0.5f;
        public const float Power = 0.4f;
    }
}
