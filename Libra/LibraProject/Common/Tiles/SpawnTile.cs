﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LibraProject.EntityBehavior;
using LibraProject.Helpers;
using WaveEngine.Common.Math;
using WaveEngine.Framework;
using WaveEngine.Framework.Physics2D;

namespace LibraProject.Common.Tiles
{
    public class SpawnTile : BaseTile
    {
        private bool _isFree = true;

        public bool IsFree
        {
            get { return _isFree; }
            set { _isFree = value; }
        }

        public SpawnTile(ETileSprite type, Vector2 position, MyScene scene)
            : base(type, position, scene)
        {
            InitFields();
        }

        public SpawnTile(BaseTile copyTile, Vector2 position)
            : base(copyTile, position)
        {
            InitFields();
        }

        private void InitFields()
        {
            RectCollider = new RectangleCollider();
            Entity.AddComponent(RectCollider);
            Entity.AddComponent(new TileSpawnBehavior(this));
        }
    }
}
