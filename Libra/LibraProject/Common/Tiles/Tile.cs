﻿using System;
using LibraProject.EntityBehavior;
using LibraProject.Helpers;
using WaveEngine.Common.Math;
using WaveEngine.Components.Graphics2D;
using WaveEngine.Framework;
using WaveEngine.Framework.Graphics;
using WaveEngine.Framework.Physics2D;

namespace LibraProject.Common.Tiles
{
    public class Tile : BaseTile
    {
        public Tile(ETileSprite type, Vector2 startingPosition, MyScene scene) 
            : base(type, startingPosition, scene)
        {
        }

        public Tile(Tile baseTile, Vector2 startingPosition) : base(baseTile, startingPosition)
        {
        }
    }
}
