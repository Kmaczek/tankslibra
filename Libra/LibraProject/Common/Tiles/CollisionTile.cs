﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LibraProject.Helpers;
using WaveEngine.Common.Math;
using WaveEngine.Framework.Physics2D;

namespace LibraProject.Common.Tiles
{
    public class CollisionTile : BaseTile
    {
        public CollisionTile(ETileSprite type, Vector2 position, MyScene scene) : base(type, position, scene)
        {
            this.RectCollider = new RectangleCollider();
            this.Entity.AddComponent(RectCollider);
        }

        public CollisionTile(BaseTile copyTile, Vector2 position) : base(copyTile, position)
        {
            this.RectCollider = new RectangleCollider();
            this.Entity.AddComponent(RectCollider);
        }
    }
}
