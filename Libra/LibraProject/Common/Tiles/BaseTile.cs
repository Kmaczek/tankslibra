﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LibraProject.Helpers;
using WaveEngine.Common.Math;
using WaveEngine.Components.Graphics2D;
using WaveEngine.Framework;
using WaveEngine.Framework.Graphics;
using WaveEngine.Framework.Physics2D;

namespace LibraProject.Common.Tiles
{
    public abstract class BaseTile : BaseEntity
    {
        public static int Width = 40;
        public static int Height = 40;

        public static int HeightOffset = Height / 2;
        public static int WidthOffset = Width / 2;
        public MyScene Scene { get; set; }

        protected BaseTile(ETileSprite type, Vector2 position, MyScene scene)
        {
            Scene = scene;
            InitBasicFields(type, position);
        }

        /// <summary>
        /// Copy constructor
        /// </summary>
        /// <param name="copyTile"></param>
        /// <param name="position"></param>
        protected BaseTile(BaseTile copyTile, Vector2 position)
        {
            this.Scene = copyTile.Scene;
            this.Name = String.Format("tile_{0}_{1}", position.X, position.Y);
            this.Sprite = (Sprite)copyTile.Sprite.Clone();
            this.Transform = new Transform2D { X = position.X, Y = position.Y, DrawOrder = DrawOrder.Tiles };
            //this.RectCollider = copyTile.RectCollider;

            this.Entity = new Entity(Name)
                .AddComponent(Sprite)
                .AddComponent(new SpriteRenderer(DefaultLayers.Alpha))
                .AddComponent(Transform);

        }

        private void InitBasicFields(ETileSprite type, Vector2 position)
        {
            this.Sprite = SpriteHelper.GetTileSprite(type);
            this.Sprite.IsGlobalAsset = true;
            this.Name = String.Format("tile_{0}_{1}", position.X, position.Y);
            this.Transform = new Transform2D()
            {
                X = position.X,
                Y = position.Y,
                DrawOrder = DrawOrder.Tiles
            };

            Entity = new Entity(Name)
                .AddComponent(Sprite)
                .AddComponent(new SpriteRenderer(DefaultLayers.Alpha))
                .AddComponent(Transform);


            //Rectangle = new Rectangle((int) startingPosition.X, (int) startingPosition.Y, TileBase.TileWidth, TileBase.TileHeight);
            //Entity.AddComponent(Transform);
        }
    }
}
