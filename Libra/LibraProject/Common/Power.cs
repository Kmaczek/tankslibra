﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LibraProject.EntityBehavior;
using LibraProject.Helpers;
using LibraProject.PowerActions;
using WaveEngine.Components.Graphics2D;
using WaveEngine.Framework;
using WaveEngine.Framework.Graphics;
using WaveEngine.Framework.Physics2D;

namespace LibraProject.Common
{
    public class Power : BaseEntity
    {
        public event PowerExpiredHandler Expired;

        protected virtual void OnPowerExpired(PowerActionEventArgs eventargs)
        {
            PowerExpiredHandler handler = Expired;
            if (handler != null) handler(this, eventargs);
        }

        public ActionBase PowerAction { get; set; }
        public MyScene Scene { get; set; }

        public Power(String name)
        {
            Name = name;
            base.Sprite = new Sprite(PathHelper.ContentPathFactory(PathType.Power, "yellow_star"));
            base.RectCollider = new RectangleCollider();
            base.Transform = new Transform2D()
                {
                    X = 300,
                    Y = 300,
                    DrawOrder = DrawOrder.Power
                };

            Entity = new Entity(Name)
                .AddComponent(Sprite)
                .AddComponent(new SpriteRenderer(DefaultLayers.Alpha))
                .AddComponent(RectCollider)
                .AddComponent(Transform);

            PowerAction = new ModifySpeed(150);
            PowerAction.Expired += PowerActionOnExpired;
        }

        private void PowerActionOnExpired(object sender, PowerActionEventArgs eventArgs)
        {
            OnPowerExpired(new PowerActionEventArgs()
            {
                Power = this
            });
        }

        public void AddToMap(MyScene scene)
        {
            Scene = scene;
            
            var behavior = new PowerBehavior(Scene, PowerAction);
            Entity.AddComponent(behavior);
        }
    }
}
