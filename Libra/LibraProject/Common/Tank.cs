﻿using System;
using LibraProject.Common.Tiles;
using LibraProject.Device;
using WaveEngine.Common.Math;
using WaveEngine.Framework;

namespace LibraProject.Common
{
    using Helpers;
    using WaveEngine.Components.Graphics2D;
    using WaveEngine.Framework.Graphics;
    using WaveEngine.Framework.Physics2D;

    public delegate void TankHitHandler(object sender, EventArgs args);
    public delegate void ControlsSetHandler(object sender, EventArgs args);

    public class TankEntity : BaseEntity
    {
        public Facing Facing { get; set; }
        public ControlState KeySet
        {
            get
            {
                return _eKeyBinding;
            }
            set
            {
                _eKeyBinding = value;

                // if selected controller is Pad
                if (_eKeyBinding.Binding == EKeyBinding.Pad)
                {
                    // initialize Pad
                    Pad = new Pad(KeySet.PadGuid);
                }

                OnControlsSet();
            }
        }

        // Occurs when tank was hitted by bullet
        public event TankHitHandler TankHit;
        // Occurs when controls has been set
        public event ControlsSetHandler ControlsSet;

        private ControlState _eKeyBinding;
        public Pad Pad;

        public TankEntity(string name, ETankSprite tankSprite)
        {
            this.Facing = Facing.N;

            base.Name = name;
            base.Sprite = SpriteHelper.GetTankSprite(tankSprite);
            base.RectCollider = new RectangleCollider() { UpdateOrder = 0.1f };
            base.Transform = new Transform2D()
                {
                    Origin = new Vector2(0.5f, 0.5f),
                    DrawOrder = DrawOrder.Tank
                };

            Entity = new Entity(Name)
                .AddComponent(Transform)
                .AddComponent(Sprite)
                .AddComponent(new SpriteRenderer(DefaultLayers.Alpha))
                .AddComponent(RectCollider);
            Entity.RefreshDependencies();
        }


        public void SetStartingPosition(Vector2 coordinates)
        {
            this.Transform.X = coordinates.X + BaseTile.Width/2;
            this.Transform.Y = coordinates.Y + BaseTile.Height/2;
        }

        /// <summary>
        /// Invokes TankHit event with projectile as sender. Add Event args to arguments
        /// if needed.
        /// </summary>
        /// <param name="projectile">Projectile that tank was hit with.</param>
        public void PerformTankShotEvent(Projectile projectile)
        {
            OnTankHit(projectile);
        }

        protected virtual void OnTankHit(Projectile projectile)
        {
            TankHitHandler handler = TankHit;
            if (handler != null) handler(projectile, null);
        }

        protected virtual void OnControlsSet()
        {
            ControlsSetHandler handler = ControlsSet;
            if (handler != null) handler(this, EventArgs.Empty);
        }
    }
}
