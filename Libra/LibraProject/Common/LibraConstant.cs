﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibraProject.Common
{
    public static class LibraConstant
    {
        public static int Width = 1280;
        public static int Height = 720;

        public static int PlayerLimit = 4;

        public static float StartingTankSpeed = 3.0f;
    }

}
