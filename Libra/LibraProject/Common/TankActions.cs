﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LibraProject.Device;
using WaveEngine.Common.Input;
using WaveEngine.Framework.Services;

namespace LibraProject.Common
{
    public class TankActions
    {
        public ButtonState MoveLeft { get; set; }
        public ButtonState MoveRight { get; set; }
        public ButtonState MoveTop { get; set; }
        public ButtonState MoveDown { get; set; }
        public ButtonState Shoot { get; set; }
        public ButtonState UsePower { get; set; }
        public ButtonState NextWeapon { get; set; }
        public ButtonState NextPower { get; set; }

        public TankActions(EKeyBinding keyBinding, PadState state)
        {
            switch (keyBinding)
            {
                case EKeyBinding.Arrows:
                    {
                        var keyboard = WaveServices.Input.KeyboardState;
                        MoveDown = keyboard.Down;
                        MoveLeft = keyboard.Left;
                        MoveRight = keyboard.Right;
                        MoveTop = keyboard.Up;
                        Shoot = keyboard.RightControl;
                        UsePower = keyboard.RightShift;
                        NextPower = keyboard.K;
                        NextWeapon = keyboard.L;
                        break;
                    }
                case EKeyBinding.WASD:
                    {
                        var keyboard = WaveServices.Input.KeyboardState;
                        MoveDown = keyboard.S;
                        MoveLeft = keyboard.A;
                        MoveRight = keyboard.D;
                        MoveTop = keyboard.W;
                        Shoot = keyboard.Space;
                        UsePower = keyboard.C;
                        NextPower = keyboard.Q;
                        NextWeapon = keyboard.E;
                        break;
                    }
                case EKeyBinding.Pad:
                    {
                        state.UpdateState();
                        MoveDown = state.PadKeyStates[PadButton.Down];
                        MoveLeft = state.PadKeyStates[PadButton.Left];
                        MoveRight = state.PadKeyStates[PadButton.Right];
                        MoveTop = state.PadKeyStates[PadButton.Up];
                        Shoot = state.PadKeyStates[PadButton.Cross];
                        UsePower = state.PadKeyStates[PadButton.Circle];
                        NextPower = state.PadKeyStates[PadButton.R1];
                        NextWeapon = state.PadKeyStates[PadButton.L1];
                        break;
                    }
            }
        }
    }

    public enum EKeyBinding
    {
        Arrows,
        WASD,
        Pad
    }
}
