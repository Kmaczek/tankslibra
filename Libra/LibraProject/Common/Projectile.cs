﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LibraProject.Helpers;
using LibraProject.Logic;
using LibraProject.Logic.Weapons;
using WaveEngine.Common.Math;
using WaveEngine.Components.Graphics2D;
using WaveEngine.Framework;
using WaveEngine.Framework.Graphics;
using WaveEngine.Framework.Physics2D;

namespace LibraProject.Common
{
    public class Projectile : BaseEntity
    {
        private readonly bool _isNameListDefined = false;
        private readonly string _className = typeof (Projectile).Name;
        public TankCore TankCore { get; set; }

        public WeaponBase Weapon { get; set; }

        public Projectile(WeaponBase weapon, Sprite sprite)
        {
            if (!_isNameListDefined)
            {
                UniqeIdGenerator.AddList(_className, new NameList(_className));
                _isNameListDefined = true;
            }
            
            this.Weapon = weapon;
            this.Name = UniqeIdGenerator.Next(_className);
            this.TankCore = weapon.TankCore;

            this.Sprite = sprite; //new Sprite(PathHelper.ContentPathFactory(PathType.Projectile, "bullet01"));
            this.RectCollider = new RectangleCollider();
            this.Transform = new Transform2D()
                {
                    X = TankCore.Tank.Transform.X,
                    Y = TankCore.Tank.Transform.Y,
                    Origin = new Vector2(0.5f, 0.5f)
                };

            this.Entity = new Entity(this.Name)
                .AddComponent(this.Sprite)
                .AddComponent(new SpriteRenderer(DefaultLayers.Alpha))
                .AddComponent(this.RectCollider)
                .AddComponent(this.Transform);
        }


        public void SetBehavior(Behavior behavior)
        {
            this.Entity.AddComponent(behavior);
        }
    }
}
