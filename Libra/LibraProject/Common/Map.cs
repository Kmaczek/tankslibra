﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LibraProject.Common.Tiles;
using LibraProject.Helpers;
using LibraProject.PowerActions;
using WaveEngine.Common.Math;
using WaveEngine.Framework.Managers;
using WaveEngine.Framework.Physics2D;

namespace LibraProject.Common
{
    public class Map
    {
        public List<BaseTile> Tiles
        {
            get { return _tileList; }
        }

        public List<BaseTile> CollisionTiles
        {
            get { return _collisionTiles; }
        }

        public MyScene Scene { get; set; }
        public static int MapWidth { get { return 920; }}
        public static int MapHeight { get { return 720; }}

        private readonly EntityManager _entityManager;
        private List<BaseTile> _tileList = new List<BaseTile>();
        private List<BaseTile> _collisionTiles = new List<BaseTile>();
        private BaseTile[,] _tiles;
        private int _xAmount;
        private int _yAmount;

        public Map(MyScene scene)
        {
            this.Scene = scene;
            this._entityManager = scene.EntityManager;
        }

        public void GenerateMap()
        {
            if (MapWidth == 0 || MapHeight == 0)
            {
                throw new Exception("Map Size not specified");
            }
            _xAmount = MapWidth / BaseTile.Width;
            _yAmount = MapHeight / BaseTile.Height;

            _tiles = new BaseTile[_xAmount, _yAmount];

            var colisionTile = new CollisionTile(ETileSprite.RedSteel, new Vector2(0, 0), Scene);
            var tile = new Tile(ETileSprite.GreyFloor, new Vector2(0, 0), Scene);
            //colisionTile.IsCollisionTile = true;
            var spawnSprite = new SpawnTile(ETileSprite.SpawnTile, new Vector2(0, 0), Scene);
            

            for (int y = 0; y < _yAmount; y++)
            {
                for (int x = 0; x < _xAmount; x++)
                {
                    //Spawn tiles
                    if ((x == 1 && y == 1) ||
                        (x == 1 && (y == MapHeight / BaseTile.Height - 2)) ||
                        (x == ((MapWidth / BaseTile.Width) - 2) && (y == (MapHeight / BaseTile.Height) - 2)) ||
                        (x == ((MapWidth / BaseTile.Width) - 2) && y == 1))
                    {
                        _tiles[x, y] = new SpawnTile(spawnSprite, new Vector2(x * BaseTile.Width, y * BaseTile.Height));
                        continue;
                    }

                    if (x % 2 == 0 && y == 3)
                    {
                        _tiles[x, y] = new CollisionTile(colisionTile, new Vector2(x * BaseTile.Width, y * BaseTile.Height));

                        continue;
                    }
                    if (x % 3 != 0 && y == 5)
                    {
                        _tiles[x, y] = new CollisionTile(colisionTile, new Vector2(x * BaseTile.Width, y * BaseTile.Height));
                        continue;
                    }

                    // non collision tiles
                    _tiles[x, y] = new Tile(tile, new Vector2(x * BaseTile.Width, y * BaseTile.Height));
                }
            }

            // on property change
            _tileList = GetTileList();
            _collisionTiles = GetCollisionTiles();
        }

        public void RegisterMap()
        {
            foreach (BaseTile tile in _tiles)
            {
                _entityManager.Add(tile.Entity);
            }
        }

        private List<BaseTile> GetTileList()
        {
            var retVal = new List<BaseTile>(450);
            foreach (BaseTile tile in _tiles)
            {
                if (tile != null)
                {
                    retVal.Add(tile);
                }
            }

            return retVal;
        }

        private List<BaseTile> GetCollisionTiles()
        {
            var retList = new List<BaseTile>(100);
            foreach (var tile in _tiles)
            {
                if (tile is CollisionTile)
                {
                    retList.Add(tile);
                }
            }
            return retList;
        }

        public List<SpawnTile> GetSpawnTiles()
        {
            var spawns = new List<SpawnTile>();
            foreach (var tile in Tiles)
            {
                if (tile is SpawnTile)
                {
                    spawns.Add(tile as SpawnTile);
                }
            }

            return spawns;
        }
    }
}
